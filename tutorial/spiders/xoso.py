import scrapy


class BootstrapTableSpider(scrapy.Spider):
    name = "xoso_table"

    def start_requests(self):
        urls = [
            'http://ketqua.net',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        table_tr = response.xpath('//*[@id="result_tab_mb"]//tbody/tr')
        for row in table_tr:
            # print(row.xpath('td//text()'))
            yield {
                'label': row.xpath('td[1]//text()').extract_first(),
                'data': row.xpath('td[2]//text()').extract_first()
            }